package com.b1t.game.fleeb;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Player extends GameObject {
	
	public static int velocity = 10;
	public final int playerSize = 64;
	
	private Handler handler;
	 
	public Player(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
	}

	@Override
	public void tick() {
		x += velocityX;
		y += velocityY;
		
		x = Game.clamp(x, playerSize/2, Game.WIDTH - (playerSize/2));
		y = Game.clamp(y, playerSize/2,  Game.HEIGHT - (playerSize/2));
		
		collission();
	}
	
	private void collission() {
		for (GameObject o: this.handler.objects ) {
			if (o.getId() == ID.BasicEnemy) {
				if (getBounds().intersects(o.getBounds())) {
					HUD.CURRENTHP -= 20;
					o.setVelocityX((o.getVelocityX() + 1) * -1);
					o.setVelocityY((o.getVelocityY() + 1) * -1);
				}
			}
		}
		
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(x - (playerSize/2) , y - (playerSize/2), playerSize, playerSize);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x - (playerSize/2) , y - (playerSize/2), playerSize, playerSize);
	}
	


}
