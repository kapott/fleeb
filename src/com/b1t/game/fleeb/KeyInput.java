package com.b1t.game.fleeb;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {
	
	private Handler handler;
	
	public KeyInput(Handler handler) {
		this.handler = handler;
	}

	public void keyPressed(KeyEvent k) {
		int key = k.getKeyCode();

		for (GameObject o: handler.objects) {
			if (o.getId() == ID.Player) {
				if (key == KeyEvent.VK_W || key == KeyEvent.VK_UP) o.setVelocityY(-1 * Player.velocity);
				if (key == KeyEvent.VK_S || key == KeyEvent.VK_DOWN) o.setVelocityY(Player.velocity);
				if (key == KeyEvent.VK_A || key == KeyEvent.VK_LEFT) o.setVelocityX(-1 * Player.velocity);
				if (key == KeyEvent.VK_D || key == KeyEvent.VK_RIGHT) o.setVelocityX(Player.velocity);
			}
		}
	}
	
	public void keyReleased(KeyEvent k) {
		int key = k.getKeyCode();
		
		for (GameObject o: handler.objects) {
			if (o.getId() == ID.Player) {
				if (key == KeyEvent.VK_W || key == KeyEvent.VK_UP) o.setVelocityY(0);
				if (key == KeyEvent.VK_S || key == KeyEvent.VK_DOWN) o.setVelocityY(0);
				if (key == KeyEvent.VK_A || key == KeyEvent.VK_LEFT) o.setVelocityX(0);
				if (key == KeyEvent.VK_D || key == KeyEvent.VK_RIGHT) o.setVelocityX(0);
			}
		}	
	}
}
