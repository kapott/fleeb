package com.b1t.game.fleeb;

import java.awt.Color;
import java.awt.Graphics;

public class HUD {
	
	public static int MAXHP = 5000;
	public static int CURRENTHP = 5000;
	
	private int padding = 150;
	private int healthbarHeight = 24;
	private int healthbarSize = Game.WIDTH - (padding * 2);
	
	private float pxPct = ((float) healthbarSize / (float) MAXHP );
	
	public HUD() {}
	
	public void tick() {
		Game.clamp(CURRENTHP, 0, MAXHP);
	}
	
	public void render(Graphics g) {
		g.setColor(Color.gray);
		g.fillRect(padding, Game.HEIGHT - 100 + healthbarHeight, healthbarSize, healthbarHeight);
		g.setColor(Color.red);
		g.fillRect(padding, Game.HEIGHT - 100 + healthbarHeight, (int)(pxPct * CURRENTHP), healthbarHeight);
	}

}
