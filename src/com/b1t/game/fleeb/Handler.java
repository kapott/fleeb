package com.b1t.game.fleeb;

import java.awt.Graphics;
import java.util.LinkedList;

public class Handler {

	LinkedList<GameObject> objects = new LinkedList<GameObject>();
	
	public void tick() {
		for (GameObject o: objects) o.tick();
	}
	
	public void render(Graphics g) {
		for (GameObject o: objects) o.render(g);
	}
	
	public void addObject(GameObject go) {
		this.objects.add(go);
	}
	
	public void removeObject(GameObject go) {
		this.objects.remove(go);
	}
}
