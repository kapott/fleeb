package com.b1t.game.fleeb;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class BasicEnemy extends GameObject{

	private int hSize = 12;
	private int vSize = 12;
	private Random r = new Random();
	
	public BasicEnemy(int x, int y, ID id) {
		super(x, y, id);
		
		velocityX = r.nextInt(5) + 1;
		velocityY = r.nextInt(5) + 1;
	}

	@Override
	public void tick() {	
		if (y <= 0 || y >= (Game.HEIGHT - vSize*2)) velocityY *= -1; 
		y += velocityY;
		
		if (x <= 0 || x >= Game.WIDTH) velocityX *= -1; 
		x += velocityX;
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.orange);
		g.fillRect(x - (hSize/2), y - (vSize/2), hSize, vSize);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, hSize, vSize);
	}

}
